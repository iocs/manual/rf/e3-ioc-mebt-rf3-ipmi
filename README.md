# e3-ioc-mebt-rf3-ipmi

IOC repository for RF station microTCA monitoring.

**IOC-name**: `MEBT-010:SC-IOC-307`

```
FRU Information:
  0   MCH       M4    NAT-MCH-CM
  3   mcmc1     M4    NAT-MCH-MCMC
  6   AMC2      M4    CCT AM G64/471
  7   AMC3      M4    SIS8300KU AMC
  9   AMC5      M4    IOxOS IFC-1410
 10   AMC6      M4    mTCA-EVR-300
 40   CU1       M4    Schroff uTCA CU
 51   PM2       M4    PM-AC1000
 60   Clock1    M4    MCH-Clock
 61   HubMod1   M4    MCH-PCIe
 92   AMC3-RTM  M4    SIS8300KU RTM
```

